import datetime
import random
from flask import Flask, jsonify, request, json
from flask_mysqldb import MySQL
from flask_restful import Resource
from flask_jwt_extended import JWTManager
from flask_jwt import JWT


app = Flask(__name__)

app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'libreria'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)


class Books(Resource):
    def get(self):
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM BOOKS ")
        books = cur.fetchall()
        cur.execute("SELECT id, name, email FROM users")
        user = cur.fetchall()
        return jsonify({'users': user, 'books': books})

    def post(self):
        cur = mysql.connection.cursor()
        n = random()
        id = n+1
        isbn = request.getjson()['isbn']
        title = request.getjson()['title']
        author = request.getjson()['author']
        release_date = json.request['release_date']

        cur.execute("SELECT isbn FROM books WHERE isbn = '" + str(isbn) + '"')
        existingIsbn = cur.fetchone()

        if isbn == existingIsbn:
            return jsonify({'error: this book with this isbn already exist'})
        else:
            cur.execute("INSERT INTO books (author, id, isbn, release_date, title) VALUES ('"+
                         str(author) + "", "" +
                        int(id) + "", "" +
                        int(isbn) + "", "" +
                        datetime(release_date) + "", "" +
                        str(title) + "')")
            return jsonify({'id': id})

        # how do you get an already created authorization token from a logged user?

    def delete(self):

        cur = mysql.connection.cursor()
        deletes = request.getjason()['id']

        cur.execute("SELECT id FROM books where id ='"+ int(deletes)+ '"')
        check_id = cur.fetchone()

        if check_id is not None:

            cur.execute("DELETE FROM books where id ='" + int(deletes) + '"')
            result = jsonify({'id': deletes})
        else:
            result = jsonify({'Message: this id is not in our database'})

        return result
