import random
from flask import Flask, jsonify, request, json
from flask_mysqldb import MySQL
from flask_restful import Resource
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager

app = Flask(__name__)


n = random.random()

app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'libreria'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['JWT_SECRET_KEY'] = 'secret'

mysql = MySQL(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)


class Register(Resource):
    # Este es el endpoint de registrarse, lo que hace es que crea un request
    # que deberia venir del front end donde vienen los datos en un JSON
    # para asi poder ingresarlos en la base de datos
    def post(self):
        cur = mysql.connection.cursor()
        id = n + 1
        name = request.get_json()['name']
        email = request.get_json()['email']
        password = bcrypt.generate_password_hash(request.get_json()['password']).decode('utf-8')

        if name is not None and password is not None and email is not None:

            cur.execute("INSERT INTO users ( id ,name, email, password) VALUES ('" +
                        int(id) + "", "" +
                        str(name) + "", "" +
                        str(email) + "", "" +
                        str(password) + "')")
            mysql.connection.commit()

            result = {
                'id': id,
                'name': name,
                'email': email,
                'password': password
            }
            return jsonify({'result': result}, {'message: TRUE'})
        else:
            return jsonify({'error: fields cant be empty'})
