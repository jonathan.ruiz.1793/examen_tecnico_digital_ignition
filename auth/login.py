from flask import Flask, jsonify, request, json
from flask_mysqldb import MySQL
from flask_bcrypt import Bcrypt
from flask_jwt_extended import (create_access_token)
from flask_jwt_extended import JWTManager
from flask_restful import Resource

app = Flask(__name__)

app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'libreria'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['JWT_SECRET_KEY'] = 'secret'

mysql = MySQL(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)


class Login(Resource):
    def post(self):
        cur = mysql.connection.cursor()
        email = request.get_json()['email']
        password = request.get_json()['password']

        cur.execute("SELECT FROM users where email = '" + str(email) + "'")
        rv = cur.fetchone()

        if Bcrypt.check_password_hash(rv['password'], password or email == ""):
            access_token = create_access_token(identity={'id': rv['id'], 'name': rv['name'], 'email': rv['email']})
            result = access_token
        else:
            result = jsonify({'error: Password is incorrect or user does not exist'})

        return result
