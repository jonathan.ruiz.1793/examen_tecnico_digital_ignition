from flask import Flask
from auth.register import Register
from flask_restful import Api
from auth.login import Login
from .books import Books
app = Flask(__name__)

Api.add_resource(Register, 'auth/register', methods=['POST'])

Api.add_resource(Login, 'auth/login', methods=['POST'])

Api.add_resource(Books, '/books', methods=['POST', 'GET', 'DELETE'])
